# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import invoice
from . import sale


def register():
    Pool.register(
        invoice.InvoiceLine,
        sale.SaleLine,
        module='account_invoice_discount', type_='model')
